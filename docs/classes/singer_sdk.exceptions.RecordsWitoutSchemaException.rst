﻿singer_sdk.exceptions.RecordsWitoutSchemaException
==================================================

.. currentmodule:: singer_sdk.exceptions

.. autoclass:: RecordsWitoutSchemaException
    :members:
    :special-members: __init__